use <libopenscad/mbox.scad>;
use <libopenscad/mcube.scad>;

inch = 25.4;

default_wall_thickness = inch / 8;
default_chamfer = default_wall_thickness / 4;

target_length = 95;
target_width = 60;
target_height = default_wall_thickness + 5;

default_mounting_hole_diameter = inch / 4;

gap = 1;

$fn = 90;

//slug
//mcube([target_length, target_width, target_height], color = "cyan", chamfer = default_chamfer, align = [0, 0, 1]);

holes = true;

// walls
color("orange") {
    for(x = [-1, 1]) {
        translate([x * (target_length + default_wall_thickness) / 2, 0, 0]) {
            translate([x * gap, 0, 0]) {
                chamfered_wall(dim = [default_wall_thickness, target_width + ((default_wall_thickness + gap) * 2), target_height], align = [0, 0, 1], chamfer = default_chamfer, manifold_overlap = true);
            }
        }
    }


    for(y = [-1, 1]) {
        translate([0, y * (target_width + default_wall_thickness) / 2, 0]) {
            translate([0, y * gap, 0]) {
                chamfered_wall(dim = [target_length + ((default_wall_thickness + gap) * 2), default_wall_thickness, target_height], align = [0, 0, 1], chamfer = default_chamfer, manifold_overlap = true);
            }
        }
    }
}


// floor
difference() {
    color("lightgreen") {
        chamfered_wall(dim = [target_length + ((default_wall_thickness + gap) * 2), target_width + ((default_wall_thickness + gap) * 2), default_wall_thickness], align = [0, 0, 1], chamfer = default_chamfer, manifold_overlap = true);
        
    }


    if(holes) {

        color("red") {
            for(x = [-1, 0, 1]) {
                for(y = [-1, 1]) {
                    translate([x * (target_length / 4), 0, 0]) {
                        translate([0, y * (target_width / 4), 0]) {
                            translate([0, 0, default_wall_thickness / 2]) {
                                translate([0, 0,  default_wall_thickness * .75]) {
                                    cylinder(h = default_mounting_hole_diameter * 1.5, d1 = default_mounting_hole_diameter / 2, d2 = default_mounting_hole_diameter * 2, center = true);
                                }
                                translate([0, 0, -1 * default_wall_thickness * .75]) {
                                    cylinder(h = default_mounting_hole_diameter * 1.5, d1 = default_mounting_hole_diameter * 2, d2 = default_mounting_hole_diameter / 2, center = true);
                                }
                                cylinder(h = default_mounting_hole_diameter * 1.5, d = default_mounting_hole_diameter, center = true);
                            }
                        }
                    }
                }
            }
        }
    }
}
