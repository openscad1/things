// BOF


$fn = 90;


module pompom2() {
    intersection() {
        
        difference() {
            union() {
                // outer (full/solid) shell
                rotate_extrude(angle = 300) {
                    translate([20, 0, 0]) {
                        circle(d = 20);
                    }
                }
            }

            color("red") union() {
                // inner (cutout/negative) shell
                rotate([0, 0, -1]) {
                    rotate_extrude(angle = 302) {
                        translate([20, 0, 0]) {
                            circle(d = 10);
                        }
                    }
                }
            }
        }


        translate([0, 0, 50]) {
            // take only the top half
            cube([100, 100, 100], center = true);
        }
    }

}




module pompom3(pip = false, hook = false, profile = 0.5, rad = 20, ring = 15, angle = 280) {

/*    rad = 20;
    ring = 15;

    angle = 280;
  */  
    intersection() {    
        union() {
            // ring
            color("red") union() {
                rotate_extrude(angle = angle) {
                    translate([rad, 0, 0]) {
                        circle(d = ring);
                    }
                }
            }
            color("blue") {
                translate([rad, 0, 0]) {
                    hull() {
                        scale([1, 0.5, 1]) {
                            sphere(d = ring);
                        }
                        if(hook) {
                            translate([ring, 0, 0]) {
                                scale([1, 0.5, 1]) {
                                    sphere(d = ring);
                                }
                            }
                        }
                    }
                }
                
                rotate([0, 0, angle]) {
                    translate([rad, 0, 0]) {
                        hull() {
                            scale([1, 0.5, 1]) {
                                sphere(d = ring);
                            }
                            if(hook) {
                                translate([ring, 0, 0]) {
                                    scale([1, 0.5, 1]) {
                                        sphere(d = ring);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        union() {
            cube([rad * 4, rad * 4, ring * profile], center = true);
        }
    }

    if(pip) {
        intersection() {
            union() {
                color("fuchsia") {
                    translate([rad + ring, 0, 0]) {
                        hull() {
                            sphere(d = ring);
                            translate([0, 0, ring]) {
                                sphere(d = ring);
                            }
                        }
                    }
                    rotate([0, 0, angle]) {
                        translate([rad + ring, 0, 0]) {
                            hull() {
                                sphere(d = ring);
                                translate([0, 0, ring]) {
                                    sphere(d = ring);
                                }
                            }
                        }
                    }
                }
            }
            union() {
                translate([0, 0, ring * 2]) {
                    cube([hoop * 4, hoop * 4, ring * 4], center = true);
                }
            }

        }
    }
}

pompom3(profile = .125, rad = 30, ring = 25, angle = 300);




// EOF
