
include <libthread/threadlib/threadlib.scad>;

    thread("G1/2-ext", turns=10);
    
    translate([25, 25, 0]) {
        
        bolt("M40x3", turns=5, higbee_arc=30);
        translate([0, 50, 0]) {
            bolt("M40x2", turns=5, higbee_arc=30);

            translate([0, 50, 0]) {
                bolt("M40x1.5", turns=5, higbee_arc=30);
            }
        }

        translate([25, -25, 0]) {
            
            nut("M12x0.5", turns=10, Douter=16);
            
            translate([25, 25, 0]) {
                
                tap("G1/2", turns=5);
                
                translate([25, -25, 0]) {
                    
                    thread("G1/2-ext", turns=5);
                    
                }
                
            }
        }
    }

