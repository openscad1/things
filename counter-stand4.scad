include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/pegboard-defaults.scad>;

$fn = 360;

wall_thickness = 25.4 / 8;

id = 40;
od = id + (wall_thickness * 2);
chamfer = wall_thickness / 4;

socket_depth = 13;




module cup(chamfer = chamfer) {
    mtube(h = socket_depth + wall_thickness, od = od, id = id, align = [0, 0, 1], chamfer = chamfer);
    mcylinder(h = wall_thickness, d = od, align = [0, 0, 1], chamfer = chamfer);
}


module grid(plug = 1) {

/*
    for(x = [-1, 0, 1]) {
        translate([x * od, 0, 0]) {
            if(plug == 1) {
                cup();
            } else {
                scale([plug, plug, 1/plug]) {
                    hull() cup(chamfer = 0);
                }
            }
        }
    }
*/
    for(y=[-1,1]) {
        translate([0, od * .5* y, 0]) {
            for(x = [-0.5, 0.5]) {
                translate([x * od, 0, 0]) {
                    if(plug == 1) {
                        cup();
                    } else {
                        scale([plug, plug, 1/plug]) {
                            hull() cup(chamfer = 0);
                        }
                    }
                }
            }
        }
    }
}

difference() {
    color("lightblue") hull() grid();
    color("red") grid(.9);
}
color("blue") grid();

