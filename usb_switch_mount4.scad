include <libmose/mdefaults.scad>
use <libmose/mbox.scad>
use <libmose/mcube.scad>
use <libmose/mcylinder.scad>

wall = inch / 8;
chamfer = wall / 4;

switchbox_l = 61;
switchbox_h = 12 - wall/2;
switchbox_w = 27;

cover_l = 19;

cable_thickness = 5;

module frame() {
    difference() {

        union(){
            
            for(y = [-1, 1]) {
                
                translate([0, y * (cable_thickness / 2), 0]) {
                    mbox(size = [switchbox_l + wall * 2, ((switchbox_w - cable_thickness) / 2 + wall), switchbox_h + wall],
                    center = [0, y, 0],
                    wall_thickness = wall,
                    chamfer = chamfer, 
                    xpos = true,
                    xneg = true,
                    ypos = y > 0 ? true : false,
                    yneg = y < 0 ? true : false,
                    zpos = false,
                    zneg = false,
                    manifold_underlap = false,
                    manifold_overlap = false,
                    color = "lightblue"
                    );



                }

            }


        }

        pips();
        




for(x = [-1, 1])
    for(y = [-1, 1]){
        
    translate([x * (switchbox_l / 2 + wall / 2), y * (switchbox_w / 4 + (cable_thickness/2)), 0])
    rotate([90, 0, 90])
    pip();
    }
}


translate([0, 0, switchbox_h / 2]){

        translate([(switchbox_l + wall) / 2, 0, 0])
        mcube([ (wall * 1), switchbox_w + wall * 2, wall], center = [0, 0, 0], color = "indigo", chamfer = inch /32);
        
        translate([-(switchbox_l + wall) / 2, 0, 0])
        mcube([ (wall * 1), switchbox_w + wall * 2, wall], center = [0, 0, 0], color = "indigo", chamfer = inch /32);
    }

}


module pip(antipip = true, pipness = 1, square = false) {
    mcylinder(d = inch / 4, h = (pipness * wall) + 0.02, $fn = square ? 4 : 180, center = true, chamfer = (antipip ? -1 : 1) * inch / 8 / 8);
}

module pips() {
    
    union() {
        for(y = [-1, 1]) {
            translate([0, y * ((switchbox_w + wall) / 2), 0]) {

                for(x = [-1, 1]) {

                    translate([ x * (switchbox_l / 2 + wall) * 0.75, 0, 0]) {

                        rotate([90, 0, 0]) {
                            pip();
                        }
                    }
                }
        
            }
        }
    }
}







//translate([0, 0, switchbox_h / 2 + wall/2]) rotate([0, 180, 0]) frame();




module clipy() {

    color("CornflowerBlue") 
    for(i = [1, -1])
        
    hull() {
//    union() {
        translate([ i * (switchbox_l / 2 + wall*2), (switchbox_w / 4) + (cable_thickness / 2), 0]) 
        mcube(center = [0, 0, 0], size = [wall * 2, wall * 2,  wall], chamfer = inch / 8 / 8);

        translate([ i * -(switchbox_l / 2 + wall*2), -((switchbox_w / 4) + (cable_thickness / 2)), 0]) 
        mcube(center = [0, 0, 0], size = [wall * 2, wall* 2,  wall], chamfer = inch / 8 / 8);
    }

    color("Tomato") 
    for(i = [1, -1])
    union() {
        translate([ i * (switchbox_l / 2 + wall*2), (switchbox_w / 4) + (cable_thickness / 2), 0]) {
            mcube(center = [ i, 0, 0], size = [wall * 3, wall * 2,  wall], chamfer = inch / 8 / 8);
            mcube(center = [ i*.5, 0, 1], size = [wall * 4, wall,  wall * 3], chamfer = inch / 8 / 8);
            mcube(center = [ i*5, 0, 1], size = [wall , wall * 2,  wall * 3], chamfer = inch / 8 / 8);
            
        }

        translate([ i * -(switchbox_l / 2 + wall*2), -((switchbox_w / 4) + (cable_thickness / 2)), 0]) {
            mcube(center = [-i, 0, 0], size = [wall * 3, wall* 2,  wall], chamfer = inch / 8 / 8);
            mcube(center = [ -i*.5, 0, 1], size = [wall * 4, wall,  wall * 3], chamfer = inch / 8 / 8);
            mcube(center = [ -i*5, 0, 1], size = [wall , wall * 2,  wall * 3], chamfer = inch / 8 / 8);
        }
    }

        
    for(x = [1, -1])
    for(y = [1, -1])
    translate([ x * (switchbox_l / 2 + wall) * 1, y * ((switchbox_w / 4) + (cable_thickness / 2)), 0]) 
    {


rotate([0, 0, y*90*x*-1]){
        translate([0, 0, wall + switchbox_h / 2])
        rotate([90, 0, 0])
        color("blue")
        pip(antipip = false, pipness = 1, square = true);

        
        color("lavender") 
        hull() {
//        union(){
            color("orange")
            translate([0, y * wall/2, wall + switchbox_h / 2])
            rotate([90, 0, 0])
            pip(antipip  = false, pipness = 1, square = true);

            color("Crimson")
            mcube([wall * 2, wall*2, wall], center = [0, y, 1], chamfer = inch / 8 / 8);
        }


    }
}
}




//translate([0, 0, -wall/2])
translate([0, 50, wall/2])
clipy();

