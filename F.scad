$fn = 90;

linear_extrude(5) {
    text("F", size = 30, font="Courier");
}


translate([2, 26, 0])
color("red")
difference() {
    cylinder(d=10, h = 5);
    cylinder(d=5, h=15, center = true);
}