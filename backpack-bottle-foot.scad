include <libopenscad/mcylinder.scad>;
include <libopenscad/mtube.scad>;

//cutaway = 1;
//debug = true;

default_fn = 90;

bottle_diameter = 68;

inch = 25.4;

flange_depth = inch / 8;
flange_offset = 40;

wall_thickness = inch / 16;
wall_chamfer = wall_thickness / 4;

$fn = default_fn;

cap_height = inch * 1;


//stop_ring = 1;


intersection() {


    if(!is_undef(cutaway)) {
        union() {

            cube([bottle_diameter * 2, bottle_diameter * 2, cap_height * 2]);

        }
    }
    
    union() { 
        // the whole thing

        color("lightgreen") {
            // outer ring

            mtube(h = cap_height, od = bottle_diameter + wall_thickness * 2, id = bottle_diameter, chamfer = wall_chamfer, align = [0, 0, 1]);
        }


        if(!is_undef(stop_ring)) {
            color("salmon") {
                // inner ring/cuff
                
                intersection() {
                    translate([0, 0, cap_height - (flange_depth * 2 + flange_offset)]) {
                        mtube(h = flange_depth * 2, od = bottle_diameter + wall_thickness * 2 + flange_depth, id = bottle_diameter - flange_depth, chamfer = flange_depth / 2, align = [0, 0, 1]);
                    }
                    translate([0, 0, (cap_height - (flange_depth * 2 + flange_offset)) - 1]) {
                        mcylinder(d = bottle_diameter + wall_thickness, h = flange_depth * 2 + 2, color = "red", align = [0, 0, 1]);
                    }
                }
            }
        }

        color("lightblue") {
            // end cap
            mcylinder(d = bottle_diameter + wall_thickness * 2, h = wall_thickness, chamfer = wall_chamfer, align = [0, 0, 1]);
        }
    }

}