// NOSTL

include <libopenscad/mcylinder.scad>;
include <libopenscad/mtube.scad>;
include <libopenscad/mcube.scad>;

//cutaway = 1;
//debug = true;
//slice = 1;
//slice2 = 1;

default_fn = 180;


inch = 25.4;

flange_depth = inch / 8;
flange_offset = 40;

wall_thickness = inch / 16;
wall_chamfer = wall_thickness / 4;

$fn = default_fn;



stop_ring = 1;



intersection() {


    union() {
        if(!is_undef(cutaway) && cutaway) {
            union() {
                mcube([bottle_diameter * 2, bottle_diameter * 2, cap_height * 4], align = [1, 1, 0.5]);
            }
        }

        
        if(!is_undef(slice) && slice) {
            union() {
                translate([0, 0, cap_height - flange_offset]) {
                    mcylinder(d = bottle_diameter * 2, h = inch / 4, align = [0, 0, 0]);
                }
            }
        }
        
        if(!is_undef(slice2) && slice2) {
            union() {
                translate([0, 0, cap_height / 2]) {
                    mcylinder(d = bottle_diameter * 2, h = inch / 2, align = [0, 0, 0]);
                }
            }
        }

    }

    // the whole thing
    union () {
        difference() {
            // reduce end-cap thickness and establish inner chamfer
            union() { 
                // cup with too-fat end
                color("lightgreen") {
                    // outer tube
                    mtube(h = cap_height, od = bottle_diameter + wall_thickness * 2, id = bottle_diameter, chamfer = wall_chamfer, align = [0, 0, 1]);
                }
                color("lightblue") {
                    // end cap
                    mcylinder(d = bottle_diameter + wall_thickness * 2, h = wall_thickness * 8, chamfer = wall_chamfer*8, align = [0, 0, 0]);
                }
            }

            union() {
                translate([0, 0, -wall_thickness * 3])
                color("blue") {
                    mcylinder(d = bottle_diameter, h = wall_thickness * 16, chamfer = wall_chamfer * 16, align = [0, 0, 1]);
                }
            }
        }
            
        if(!is_undef(stop_ring)) {
            color("salmon") {
                // inner stop ring/cuff
                intersection() {
                    translate([0, 0, cap_height - (flange_depth * 2 + flange_offset)]) {
                        mtube(h = flange_depth * 2, od = bottle_diameter + wall_thickness * 2 + flange_depth, id = bottle_diameter - flange_depth, chamfer = flange_depth / 2, align = [0, 0, 1]);
                    }
                    translate([0, 0, (cap_height - (flange_depth * 2 + flange_offset)) - 1]) {
                        mcylinder(d = bottle_diameter + wall_thickness, h = flange_depth * 2 + 2, color = "red", align = [0, 0, 1]);
                    }
                }
            }
        }
    }

}
