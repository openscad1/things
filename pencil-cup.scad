

outer = 40;
inner = 39.9;

default_x_scale = 1;
default_y_scale = 1;
default_z_scale = 2.5;
default_color = "orange";
default_fatness = 2;

default_wall_thickness = 5;

//debug = true; 
do_minkowski = true;

$fn = is_undef(debug) ? 60 : 12;

module cup(x_scale = default_x_scale, y_scale = default_y_scale, z_scale = default_z_scale, fatness = default_fatness, color = default_color) {
    color(color) {
        scale([x_scale, y_scale, z_scale]) {
     
            intersection() {
                difference() {
                    sphere(d = outer);
                    translate([0, 0, (outer-inner) / 2]) {
                        intersection() {
                            sphere(d = inner);
                            cube([outer, outer, outer/fatness], center = true);
                        }
                    }
                }
                cube([outer, outer, outer/fatness], center = true);
            }
        }

    }
}


module render_cup() {
    
    minkowski() {
        intersection() {
            cup();
            
            if(!is_undef(debug)) {
                color("red") {
                    translate([outer/4, outer / 4, 0]) {
                cube([outer, outer, outer*2], center = true);
                    }
                }
            }
        }
        if(!is_undef(do_minkowski)) {
            sphere(d = default_wall_thickness);
        }
    } 
}

render_cup();
