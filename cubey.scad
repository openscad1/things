// NOSTL

module cubey(size = 60, chamfer_ratio = 1/6) {

  chamfer = size * chamfer_ratio;

    hull() {
        // x
        color("red") cube([size, size - (chamfer * 2), size - (chamfer * 2)], center = true);

        // y
        color("green") cube([size - (chamfer * 2), size, size - (chamfer * 2)], center = true);

        // z
        color("blue") cube([size - (chamfer * 2), size - (chamfer * 2), size], center = true);
    }
}

