pill_diameter = 7.5;
pill_height = 4;


module pill_slug() {
    cylinder(h = pill_height, d = pill_diameter, center = true);
}
    
module pills_slug() {
    for(a = [0 : 120 : 359]) {
        rotate([0, 0, a])
        translate([pill_diameter * 1, 0, 0]) {
            pill_slug();
        }
    }
}


difference() {
    union() {
        // frame
        frame_scale = 1.25;
        hull() {
            scale([frame_scale, frame_scale, 1]) {
                pills_slug();
            }
        }
    }

    union() {
        // pill cutouts
        color("red") {
            scale([1,1,2]) {
                pills_slug();
            }
        }
    }
}