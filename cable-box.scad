// NOSTL

include <libmose/mdefaults.scad>
use <libmose/mcylinder.scad>
use <libmose/mtube.scad>
use <libmose/mcube.scad>
use <libmose/mutil.scad>




default_text_size = 7;




default_wall_thickness = inch * 1/8;

default_x_extent = 3;
default_y_extent = 2;
default_z_box_extent = 1.5;
default_z_lid_extent = 0.5;

default_y_notch_width = 10;


module notched_wall(wall_extent, wall_thickness, z_extent, notch_extent = undef, shoulder_extent = undef) {
    
    if(is_undef(notch_extent) != is_undef(shoulder_extent)) {
        assert(false, "notch_extent and shoulder_extent must be provided or omitted as a pair");
    }

    
    chamfer = wall_thickness  / 4;


    if(notch_extent) {
        
        mcube([wall_extent * inch, wall_thickness, shoulder_extent * inch], chamfer = chamfer, center = [0, 0, 1], color = "sandybrown");

        for(x = [-1, 1]) {
            translate([x * ((wall_extent * inch) / 2), 0, 0]) {
                mcube([((wall_extent - notch_extent) * inch) / 2, wall_thickness , z_extent * inch], chamfer = chamfer, center = [-x, 0, 1], color = "slateblue");
            }
        }
        
    } else {
        mcube([wall_extent * inch, wall_thickness , z_extent * inch], chamfer = chamfer, center = [0, 0, 1], color = "plum");
    }

}


module floor(x, y, r, c, w) {
        color("lightblue") hull() {
            translate([ (x * 0.5) - (r), (y * 0.5) - r, 0]) 
            mcylinder(d = r * 2, h = w, chamfer = c, center = [0, 0, 1]);
            
            translate([ -1 * ((x * 0.5) - (r)), (y * 0.5) - r, 0]) 
            mcylinder(d = r * 2, h = w, chamfer = c, center = [0, 0, 1]);
            
            translate([-1 * ((x * 0.5) - (r)), -1 * ((y * 0.5) - r), 0]) 
            mcylinder(d = r * 2, h = w, chamfer = c, center = [0, 0, 1]);
            
            translate([1 * (x * 0.5) - (r), -1 * ((y * 0.5) - r), 0]) 
            mcylinder(d = r * 2, h = w, chamfer = c, center = [0, 0, 1]);
            
    }
}

module box(x_extent = default_x_extent, y_extent = default_y_extent, z_extent = default_z_box_extent, x_gap = 0, y_notch_width = default_y_notch_width, corner_radius = 10, wall_thickness = default_wall_thickness, center = true, $fn = 180) {
    
    chamfer = wall_thickness / 4;

    
for(x = [-1, 1]) {
    for(y = [x, -x]) {

        p1 = x < 0 ? 0 : 2;
        p2 = x < 0 ? (y < 0 ? 0 : 1) : (y > 0 ? 0 : 1);
        p3 = p1 + p2;
        r = (45 + 180) - (90 * p3);
        echo(p1, p2, p3, r, x, y);
        

    translate([ x * ((inch * x_extent * 0.5) - (corner_radius)), y * ((inch * y_extent * 0.5) - corner_radius), 0]) 
    rotate([0, 0,  r]) 
    mtube(h = (inch * z_extent), od = corner_radius * 2, id = corner_radius * 2 - wall_thickness * 2, center = [0, 0, 1], chamfer = chamfer, angle = 90, chamfer_ends = false);
        
        
/*
    translate([ -1 * ((inch * x_extent * 0.5) - (corner_radius)), (inch * y_extent * 0.5) - corner_radius, 0]) 
    rotate([0, 0, 135]) 
    mtube(h = (inch * z_extent), od = corner_radius * 2, id = corner_radius * 2 - wall_thickness * 2, center = [0, 0, 1], chamfer = chamfer, angle = 90, chamfer_ends = false);

    translate([-1 * ((inch * x_extent * 0.5) - (corner_radius)), -1 * ((inch * y_extent * 0.5) - corner_radius), 0]) 
    rotate([0, 0, 225]) 
    mtube(h = (inch * z_extent), od = corner_radius * 2, id = corner_radius * 2 - wall_thickness * 2, center = [0, 0, 1], chamfer = chamfer, angle = 90, chamfer_ends = false);
    
    translate([1 * (inch * x_extent * 0.5) - (corner_radius), -1 * ((inch * y_extent * 0.5) - corner_radius), 0]) 
    rotate([0, 0, 315]) 
    mtube(h = (inch * z_extent), od = corner_radius * 2, id = corner_radius * 2 - wall_thickness * 2, center = [0, 0, 1], chamfer = chamfer, angle = 90, chamfer_ends = false);
    */
    }
}

    floor(inch * x_extent, inch * y_extent, corner_radius, chamfer, wall_thickness);

    
    color("orange") {
        
        translate([0, (inch * y_extent * 0.5) * 1 - wall_thickness / 2, 0])  {
            notched_wall(wall_extent = x_extent - ((corner_radius - chamfer) * 2 / inch), wall_thickness = wall_thickness, z_extent = z_extent, shoulder_extent = wall_thickness * 2 / inch, notch_extent = y_notch_width/inch);
        }
        
        translate([0, (inch * y_extent * 0.5) * -1 + wall_thickness/2, 0]) {
            notched_wall(wall_extent = x_extent - ((corner_radius - chamfer) * 2 / inch), wall_thickness = wall_thickness, z_extent = z_extent);
        }        

    }


    
    
    {

        
        translate([ 1 * ((inch * x_extent * 0.5) ) - wall_thickness / 2, 0, 0]) {
            rotate([0, 0, 90]) {
                notched_wall(wall_extent = y_extent - ((corner_radius - chamfer) * 2 / inch), wall_thickness = wall_thickness, z_extent = z_extent, shoulder_extent = wall_thickness * 2 / inch, notch_extent = y_notch_width/inch);
            }
        }


        translate([ -1 * ((inch * x_extent * 0.5)) + wall_thickness / 2, 0, 0]) {
            rotate([0, 0, 90]) {
                notched_wall(wall_extent = y_extent - ((corner_radius - chamfer) * 2 / inch), wall_thickness = wall_thickness, z_extent = z_extent, shoulder_extent = wall_thickness * 2 / inch, notch_extent = y_notch_width/inch);
            }

        }

    }


}

module box_lid(x_extent = default_x_extent, y_extent = default_y_extent, z_extent = default_z_lid_extent, x_gap = 0, y_notch_width = default_y_notch_width, corner_radius = 10, wall_thickness = default_wall_thickness, center = true, $fn = 180) {

    
    chamfer = wall_thickness / 4;

    for(x = [-1, 1]) {
    for(y = [x, -x]) {

        p1 = x < 0 ? 0 : 2;
        p2 = x < 0 ? (y < 0 ? 0 : 1) : (y > 0 ? 0 : 1);
        p3 = p1 + p2;
        r = (45 + 180) - (90 * p3);
        echo(p1, p2, p3, r, x, y);

    translate([ x * ((inch * x_extent * 0.5) - (corner_radius)), y * ((inch * y_extent * 0.5) - corner_radius), 0]) 
    rotate([0, 0,  r]) 
    mtube(h = (inch * z_extent), od = corner_radius * 2 - wall_thickness * 2, id = corner_radius * 2 - wall_thickness * 2 - wall_thickness* 2, center = [0, 0, 1], chamfer = chamfer, angle = 90, chamfer_ends = false);
        
/*
        
    translate([ -1 * ((inch * x_extent * 0.5) - (corner_radius)), (inch * y_extent * 0.5) - corner_radius, 0]) 
    rotate([0, 0, 135]) 
    mtube(h = (inch * z_extent), od = corner_radius * 2 - (wall_thickness * 2), id = corner_radius * 2 - wall_thickness * 2 - wall_thickness * 2, center = [0, 0, 1], chamfer = chamfer, angle = 90, chamfer_ends = false);

    translate([-1 * ((inch * x_extent * 0.5) - (corner_radius)), -1 * ((inch * y_extent * 0.5) - corner_radius), 0]) 
    rotate([0, 0, 225]) 
    mtube(h = (inch * z_extent), od = corner_radius * 2 - (wall_thickness * 2), id = corner_radius * 2 - wall_thickness * 2 - (wall_thickness * 2), center = [0, 0, 1], chamfer = chamfer, angle = 90, chamfer_ends = false);
    
    translate([1 * (inch * x_extent * 0.5) - (corner_radius), -1 * ((inch * y_extent * 0.5) - corner_radius), 0]) 
    rotate([0, 0, 315]) 
    mtube(h = (inch * z_extent), od = corner_radius * 2 - (wall_thickness * 2), id = corner_radius * 2 - wall_thickness * 2 - (wall_thickness * 2), center = [0, 0, 1], chamfer = chamfer, angle = 90, chamfer_ends = false);
    */
}
}
    floor(inch * x_extent, inch * y_extent, corner_radius, chamfer, wall_thickness);

    
    color("orange") 
        translate([0, - wall_thickness, 0]) 
        translate([0, (inch * y_extent * 0.5) * 1 - wall_thickness / 2, 0])
        notched_wall(wall_extent = x_extent - ((corner_radius - chamfer) * 2 / inch), wall_thickness = wall_thickness, z_extent = z_extent, shoulder_extent = wall_thickness * 2 / inch, notch_extent = y_notch_width/inch);

        translate([0, - wall_thickness / 2, 0]) 

        translate([0, (inch * y_extent * 0.5) * 1 - wall_thickness / 2, 0])
                color("fuchsia")    mcube([y_notch_width, wall_thickness * 2, (wall_thickness * 2)], chamfer = chamfer, center = [0,  0, 1] );



//        translate([0, (inch * y_extent * 0.5 - wall_thickness) *  1, 0]) 
//        mcube([inch * x_extent - (corner_radius * 2) + (chamfer * 2) - (wall_thickness * 0), wall_thickness, inch * z_extent], chamfer = chamfer, center = [0, -1, 1] );
        color("orange")
        translate([0, (inch * y_extent * 0.5 - wall_thickness) * -1, 0]) 
        mcube([inch * x_extent - (corner_radius * 2) + (chamfer * 2) - (wall_thickness * 0), wall_thickness, inch * z_extent], chamfer = chamfer, center = [0,  1, 1] );

    


    
    
    {

        
        translate([ 1 * ((inch * x_extent * 0.5 - wall_thickness) ), 0, 0]) {
            color("salmon")    mcube([wall_thickness, (inch * y_extent) - (corner_radius * 2) + (chamfer * 2) - (wall_thickness * 2), (wall_thickness * 2)], chamfer = chamfer, center = [-1,  0, 1] );
            
        color("fuchsia")    mcube([wall_thickness * 2, y_notch_width, (wall_thickness * 2)], chamfer = chamfer, center = [0,  0, 1] );


            color("cyan") translate([0, (inch * y_extent * 0.5) - corner_radius + chamfer, 0]) 
            mcube([wall_thickness, (y_extent * inch / 2) - corner_radius + chamfer - y_notch_width / 2, inch * z_extent      ], chamfer = chamfer, center = [-1, -1, 1] );
            
            color("cyan") translate([0, ((inch * y_extent * 0.5)  - corner_radius + chamfer) * -1, 0]) 
            mcube([wall_thickness, (y_extent * inch / 2) - corner_radius + chamfer - y_notch_width / 2, inch * z_extent      ], chamfer = chamfer, center = [-1, 1, 1] );
        }


        translate([ -1 * ((inch * x_extent * 0.5 - wall_thickness) ), 0, 0]) {
            color("salmon")    mcube([wall_thickness, (inch * y_extent) - (corner_radius * 2) + (chamfer * 2) - (wall_thickness * 2), (wall_thickness * 2)], chamfer = chamfer, center = [1,  0, 1] );

        color("fuchsia")    mcube([wall_thickness * 2, y_notch_width, (wall_thickness * 2)], chamfer = chamfer, center = [0,  0, 1] );


            color("cyan") translate([0, (inch * y_extent * 0.5) - corner_radius + chamfer, 0]) 
            mcube([wall_thickness, (y_extent * inch / 2) - corner_radius + chamfer - y_notch_width / 2, inch * z_extent      ], chamfer = chamfer, center = [1, -1, 1] );
            
            color("cyan") translate([0, ((inch * y_extent * 0.5)  - corner_radius + chamfer) * -1, 0])
            mcube([wall_thickness, (y_extent * inch / 2) - corner_radius + chamfer - y_notch_width / 2, inch * z_extent    ], chamfer = chamfer, center = [1, 1, 1] );
            
            
        }

    }


}




box();

//translate([0, 0, inch * default_z_box_extent + default_wall_thickness]) rotate([180, 0, 0])
translate([0, inch * (default_y_extent + 1), 0])
{ 
//    color("red")  

    box_lid(); 
}


//notched_wall(wall_extent = 4, wall_thickness = 1/4 * inch, z_extent = 3, shoulder_extent = 1/2, notch_extent = 1);

//translate([0, inch, 0]) 
//notched_wall(wall_extent = 4, wall_thickness = 1/4 * inch, z_extent = 3);
