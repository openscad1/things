use <libopenscad/mcube.scad>;
use <libopenscad/mcylinder.scad>;
include <libthread/threadlib/threadlib.scad>;

    
$fn = 90;


//cutaway = 1;
//if(!is_undef(cutaway)) {

//plug = 1;
//if(!is_undef(plug)) {




default_thread_size = 20;

default_douter = default_thread_size * 1.25;

default_bolt_scale = 0.9875;
default_bolt_stretch = 1.25;  // to avoid the need for supporting material by keeping the overhang from being more than 45 degrees

default_nut_turns = 10;
default_nut_color = "fuchsia";

default_fn = 100;

module mnut(thread_size = default_thread_size, nut_turns = default_nut_turns, douter = default_douter, bolt_stretch = default_bolt_stretch, color = default_nut_color, $fn = default_fn) {


    thread_pattern = str("M", thread_size);

/*
    module _flute(d) {
        cylinder(d = default_diameter * d, h = H, center = true, $fn = 100);
        for(z = [-1, 1]) {
            translate([0, 0, z * ((H / 2) - chamfer)]) {
                cylinder(d1 = (default_diameter * d) - (chamfer * 2 * z), d2 = (default_diameter * d) + (chamfer * 2 * z), h = chamfer * 4, center = true);
            }
        }
    }

*/



    specs = thread_specs(str(thread_pattern, "-int"), table=THREAD_TABLE);
    P = specs[0];
    H = (nut_turns + 1) * P * bolt_stretch;
    wall_thickness = (douter - thread_size) / 2;

    if(!is_undef(debug)) {
        echo("P:", P);
        echo("H:", H);
        echo("douter:", douter);
        echo("thread_size:", thread_size);
    }
    
    
            difference() {

                color("fuchsia") {

                    translate([0, 0, -H/2]) {

                        translate([0, 0, (P * bolt_stretch) / 2]) { // level the bottom unthreaded bit to z plane

                            scale([1, 1, bolt_stretch]) {
                          
                                echo("nut(", thread_pattern, nut_turns, douter);
color(color){
    nut(thread_pattern, turns = nut_turns, Douter=douter);
}
                          
                            }
                      
                        }
                  
                    }

                    }

                    
                    echo("wall_thickness:", wall_thickness);


union(){
    //chamfer thread hole
    
    translate([0, 0, (H / 2) + wall_thickness*.75]) { 
        // top
        mcylinder(d1 = douter - (wall_thickness * 4), d2 = douter, h = wall_thickness*2, align = [0, 0, -1]);
    }

    translate([0, 0, -((H / 2) + wall_thickness*.75 )]) {
        // bottom
        mcylinder(d2 = douter - (wall_thickness * 4), d1 = douter, h = wall_thickness*2, align = [0, 0, 1]);
    }

}
        }




/*
                color("red") {
                    for(z = [0 : 60 : 359]) {
                        rotate([0, 0, z]) {
                            translate([(douter * 1.25) / 2, 0, 0]) {
                                _flute(1/6);
                            }
                        }
                    }
                }

            }
        }



color("royalblue") {
            hull() {
                cylinder(d= douter  , h = H - chamfer * 2, center = true);
                cylinder(d= douter - chamfer * 2 , h = H , center = true);

            }
        }
    }
    */

}


mnut();

module mountie_bolt(
        reference_diameter = default_diameter,
        reference_height_ratio = default_height_ratio,
        thread_size, 
        thread_pattern, 
        bolt_turns, 
        bolt_stretch) {

    height        = reference_diameter * reference_height_ratio;
    echo("height: ", height);

    rotate([90, 0, 0]) {

        intersection() {
            color("orange") {
                scale([bolt_scale, bolt_scale, bolt_stretch]) {
                    bolt(thread_pattern, turns=bolt_turns, higbee_arc=30);
                }
            }

            color("red") {
                translate([0, 0, -height]) {
                    mcube([thread_size * bolt_scale, (thread_size * 2/3) * bolt_scale, height * bolt_turns / 2], align = [0, 0, 1]);
                }
            }
        }
    }
}


