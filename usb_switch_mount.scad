include <libmose/mdefaults.scad>
use <libmose/mbox.scad>

wall = inch / 8;
chamfer = wall / 4;

switchbox_l = 61;
switchbox_h = 14;
switchbox_w = 26;

cover_l = 19;

cable_thickness = 5;

for(y = [-1, 1]) {
    translate([0, y * (cable_thickness / 2), 0]) {


        mbox(size = [switchbox_l + wall * 2, ((switchbox_w - cable_thickness) / 2 + wall), switchbox_h],
            center = [0, y, 1],
            wall_thickness = wall,
            chamfer = chamfer, 
            xpos = true,
            xneg = true,
            ypos = y > 0 ? true : false,
            yneg = y < 0 ? true : false,
            zpos = false,
            zneg = false,
            manifold_underlap = false,
            manifold_overlap = false,
            color = "orange"
        );

        for(x = [-1, 1]) {

            translate([ x * (switchbox_l / 2 + wall), 0, 0]) {

        mbox(size = [cover_l + wall , ((switchbox_w - cable_thickness) / 2 + wall), switchbox_h + wall],
            center = [-x, y, 1],
            wall_thickness = wall,
            chamfer = chamfer, 
            xpos = x > 0 ? true : false,
            xneg = x < 0 ? true : false,
            ypos = y > 0 ? true : false,
            yneg = y < 0 ? true : false,
            zpos = false,
            zneg = false,
            manifold_underlap = false,
            manifold_overlap = false,
            color = "lightgreen"
        );
            }
        }
    }
}


for(x = [-1, 1]) {
    translate([x * (switchbox_l / 2 + wall), 0, 0]) {
        mbox(size = [cover_l + wall, switchbox_w + wall * 2, switchbox_h + wall * 1],
            center = [-x, 0, 1],
            wall_thickness = wall,
            chamfer = chamfer, 
            xpos = false,
            xneg = false,
            ypos = false,
            yneg = false,
            zpos = true,
            zneg = false,
            manifold_underlap = false,
            manifold_overlap = false,
            color = "pink"
        );
    }
}