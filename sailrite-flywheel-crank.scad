overall_length = 50;
step_length = 7.9;

fat_end = 40;
skinny_end = 20;

shaft = 8.1;
bell_end = 15;

chamfer = 2.5;

use <libopenscad/mcylinder.scad>
use <libopenscad/mtube.scad>
use <libopenscad/mcube.scad>

$fn = 180;

inner_sleeve = 1;
center_sleeve = 1;
outer_sleeve = 1;

//cutaway = 1;
//cutaway2 = 1;

//debug = 1;

difference() {
    union() {
        // the whole thing
        
        
            union() {
                translate([0, 0, -overall_length / 2]) {
                            if(!is_undef(inner_sleeve)) {

                    color("lightgreen") {
                        mtube(h = step_length * 2, od = skinny_end, id = shaft, chamfer = (skinny_end - shaft) / 8);
                    }
                    color("cyan") {
                        mtube(h = step_length, od = skinny_end, id = shaft, align = [0, 0, 1]);
                    }

                }
                   if(!is_undef(debug)) {
                        color("fuchsia") {
                            difference() {
                            mtube(h = step_length, od = skinny_end*2, id = shaft, align = [0, 0, -1]);
                                if(!is_undef(cutaway2)) {
            mcube([overall_length * 2, overall_length * 2, overall_length * 2], align = [-0.9, -0.9, 0], color = "red");
        }
    }

                        }
                    }
            }
        }
                 

        if(!is_undef(outer_sleeve)) {
            union() {
            translate([0, 0, overall_length / 2])
                color("lightblue") {
                    mtube(h = step_length * 2, od = fat_end, id = bell_end, chamfer =  (fat_end - bell_end) / 8);
                }
            }
        }


        if(!is_undef(center_sleeve)) {
            
            
            
            difference() {
                union() {            
                    hull() {
                        translate([0, 0, -(overall_length - (step_length * 2)) / 2]) {
                            union() {
                                difference() {
                                    color("green") {
                                        mtube(h = step_length * 2, od = skinny_end, id = bell_end, chamfer = chamfer);
                                    }
                                    if(!is_undef(cutaway)) {
                                        mcube([overall_length * 2, overall_length * 2, overall_length * 2], align = [-1, -1, 0], color = "red");
                                    }
                                }
                            }
                        }


                        translate([0, 0, (overall_length - (step_length * 2)) / 2]) {
                            union() {
                                difference() {
                                    color("blue") {
                                        mtube(h = step_length * 2, od = fat_end, id = bell_end, chamfer = chamfer);
                                    }
                                    if(!is_undef(cutaway)) {
                                        mcube([overall_length * 2, overall_length * 2, overall_length * 2], align = [-1, -1, 0], color = "red");
                                    }
                                }
                            }
                        }

                    } //hull
                }

                // cut out center cylinder
                union() {
                    mcylinder(d = bell_end, h = overall_length + (step_length * 2) + 10, color = "red");
                }

            } //difference
        }
    }

    if(!is_undef(cutaway2)) {
        union() {
            mcube([overall_length * 2, overall_length * 2, overall_length * 2], align = [-1, -1, 0], color = "red");
        }
    }

}

    