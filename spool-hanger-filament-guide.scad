use <libopenscad/mtube.scad>;
use <libopenscad/mcube.scad>;
use <libopenscad/mcylinder.scad>;

$fn = 90;

inch = 25.4;

interior_width = inch * 0.31;
exterior_width = interior_width + inch * 0.25;




clip_height = inch * 1.0;



module horseshoe(height, id, od, foot, align = [0, 0, 0], color = undef, chamfer = -1, debug = undef) {

    chamfer = (od - id) / 8;

    module thing() {


        color("orange") {
            rotate([0, 0, 180]) {
                mtube(h = height, od = od, id = id, chamfer = chamfer,  angle = 180);
            }
        }
        
        color("lightblue") {
            for(y = [-1, 1]) {
                translate([0, y * od / 2, 0]) {
                    hull() {
                        mcylinder(h = height, d = (od - id) / 2, align = [0, -y, 0], chamfer = chamfer);
                        translate([foot, 0, 0]) {
                            mcylinder(h = height, d = (od - id) / 2, align = [0, -y, 0], chamfer = chamfer);
                        }
                    }
                }
            }
        }

    }

    translate([(od / 2) * align.x, (od / 2) * align.y, (height / 2) * align.z]) {
        if(debug != undef) {
            color("red") {
                cube([id, id, height * 1.5], center = true);
            }
        }

        if (color == undef) {
            thing();
        } else {
            color(color) {
                thing();
            }
        }
    }
}


module oval(color = undef) {
    // a pair of horseshoes facing each other to make an oval

    rotate([0, 0, -90]) {
        // down-facing horseshoe
        horseshoe(height = clip_height, id = interior_width, od = exterior_width, foot = 7, align = [1, 0, 0], color = color ? color : "pink");
    }

    translate([0, -(exterior_width - interior_width), 0]) { // align interior edges
        
        translate([0, -(11.75 + 4), 0]) { // 11.75mm bolt height, 2mm top and bottom to allow for curvature
            rotate([0, 0, 90]) {
                horseshoe(height = clip_height, id = interior_width, od = exterior_width, foot = 7, align = [1, 0, 0], color = color ? color : "orange");
            }
        }
    }
}



bolt_bracket_height = ((11.75 + 4) + (exterior_width - interior_width));

            finger_width = 5;
            finger_length = 50;

filament_gap = finger_width;


difference() {
    union() {

        // through hole for bolt
        translate([- ((exterior_width / 2) - (exterior_width - interior_width) / 4), bolt_bracket_height/2, 0]) {
            oval();
        }




        difference() {


            // filament fingers


            difference() {
                union() {
                    for( z = [-1, 1]) {
                        translate([0, 0, z * 0.1]) {
                            mcube(size = [finger_length * 2, finger_width * 3, bolt_bracket_height/2 + (exterior_width - interior_width) / 4], align = [0, 0, z], chamfer = finger_width , color = "cyan");
                        }
                    }
                }
                union() {
                    // cutout between fingers 
                    translate([finger_width, 0, 0]) {
                        mcube([finger_length - finger_width * 2.5, finger_width * 3, filament_gap], align = [1, 0, 0], color = "red");
                    }
                    
                    // clip the negative half of the fingers
                    mcube([100, 100, 100], align = [-1, 0, 0], color = "red");
                    

                }
            }



            // take just the bottom half

        }
    }


    mcube([101, 101, 101], align = [0, 0, 1], color = "red");
}
