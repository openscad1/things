$fn = 90;

linear_extrude(5) {
    text("A", size = 30, font="Courier");
}


translate([12, 27, 0])
color("red")
difference() {
    cylinder(d=10, h = 5);
    cylinder(d=5, h=15, center = true);
}