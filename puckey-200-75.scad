size = 20;
chamfer = size / 6;


use <libopenscad/mcylinder.scad>



difference() {
    union() {
        color("lightgreen") {
            mcylinder(h = 20, d = 60, d1 = 0, d2 = 0, center = false, align = [0, 0, 0], chamfer = 2.5, $fn = 180);
        }
    }


    union() {
//        temp = 190; flow = 110;
        temp = 200; flow = 75;

        color("fuchsia")
        translate([0, 0, 5])
        linear_extrude(30) {
            s = 10;
            translate([0,  2, 0]) text(str("t:", temp), size = s, font="Courier", halign = "center", valign = "bottom");
            translate([0, -2, 0]) text(str("f:", flow), size = s, font="Courier", halign = "center", valign = "top");
        }

    }


}
