use <cable-box.scad>


inch = 25.4;


box(x_extent = 6, y_extent = 5, z_extent = 3, corner_radius = 15, y_notch_width = inch * 3/4, wall_thickness = inch / 4);
