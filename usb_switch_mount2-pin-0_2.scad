include <libmose/mdefaults.scad>
use <libmose/mtube.scad>


//translate([ 0,  0, 0]) color("lightblue") scale([0.9,1,1]) mtube(h = 9, od = inch / 4, id = inch / 8, align = [0, 0, 0], chamfer = inch / 8 / 6, $fn = 180);

translate([ 0, 10, 0]) color("lightgreen") mtube(h = 9, od = inch / 4 + 0.2, id = inch / 8, align = [0, 0, 0], chamfer = inch / 8 / 6, angle = 300, $fn = 180);
