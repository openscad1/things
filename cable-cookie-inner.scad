use <cable-cookie.scad>

inner(h = 2 - (1/8) - orbital_allowance(), d = 3 - (1/4) - jessie_shrink(), orbit = true);
