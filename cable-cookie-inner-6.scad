use <cable-cookie.scad>

inner(h = 2 - (1/8) - orbital_allowance(), d = 6 - (1/4) - jessie_shrink(), arc = 150, orbit = true);
