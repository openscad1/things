mounting_hole_diameter = 6;
knob_diameter = mounting_hole_diameter + 2;
foot_diameter = 25.4;
foot_height = 5;
post_height = 10;
slot_width = 2;
chamfer = 1;

$fn = 90;

include <libopenscad/mcylinder.scad>
include <libopenscad/mcube.scad>


difference() {
union() {
    // positive
    color("cyan") {
        mcylinder(d = mounting_hole_diameter, h = post_height, chamfer = chamfer, align = [0, 0, 1]);
        mcylinder(d = foot_diameter, h = foot_height, chamfer = chamfer, align = [0, 0, 1]);
    }



    color("orange") {
        translate([0, 0, post_height]) {
            sphere(d = knob_diameter);
        }
    }
}

union() {
    // negative
    color("red") {
        translate([0, 0, foot_height + 0.01]) {
            mcube([foot_diameter, slot_width, post_height + foot_height], align = [0, 0, 1]);
        }
    }
}
}