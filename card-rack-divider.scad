include <libopenscad/mcube.scad>;
include <libthread/threadlib/threadlib.scad>;

len_total_front_to_back_gap = 28;
len_total_side_to_side_gap = 14;

height = 7.5;

chamfer = 1/2;

color("orange") {
    // backstop
    mcube([(len_total_side_to_side_gap / 6) * 2, (len_total_side_to_side_gap / 6) * 3, height], chamfer = chamfer, align = [1,1,1]);
}


color("lightblue") {
    // guide rail
    mcube([len_total_front_to_back_gap * 2, len_total_side_to_side_gap / 6, height], chamfer = chamfer, align = [0,1,1]);

}
