include <libmose/mdefaults.scad>
use <libmose/mbox.scad>
use <libmose/mcube.scad>
use <libmose/mcylinder.scad>

wall = inch / 8;
chamfer = wall / 4;

switchbox_l = 61;
switchbox_h = 14 - wall/2;
switchbox_w = 27;

cover_l = 19;

cable_thickness = 5;

module frame() {
    difference() {

        union(){
            
            for(y = [-1, 1]) {
                
                translate([0, y * (cable_thickness / 2), 0]) {
                    mbox(size = [switchbox_l + wall * 2, ((switchbox_w - cable_thickness) / 2 + wall), switchbox_h + wall],
                    center = [0, y, 0],
                    wall_thickness = wall,
                    chamfer = chamfer, 
                    xpos = true,
                    xneg = true,
                    ypos = y > 0 ? true : false,
                    yneg = y < 0 ? true : false,
                    zpos = false,
                    zneg = false,
                    manifold_underlap = false,
                    manifold_overlap = false,
                    color = "lightblue"
                    );



                }

            }


        }

        pips();

    }

    translate([0, 0, switchbox_h / 2]){

        translate([(switchbox_l + wall) / 2, 0, 0])
        mcube([ (wall * 1), switchbox_w + wall * 2, wall], center = [0, 0, 0], color = "indigo", chamfer = inch /32);
        
        translate([-(switchbox_l + wall) / 2, 0, 0])
        mcube([ (wall * 1), switchbox_w + wall * 2, wall], center = [0, 0, 0], color = "indigo", chamfer = inch /32);
    }

}


module pip(antipip = true, pipness = 1, square = false) {
    mcylinder(d = inch / 4, h = (pipness * wall) + 0.02, $fn = square ? 4 : 180, center = true, chamfer = (antipip ? -1 : 1) * inch / 8 / 8);
}

module pips() {
    
    union() {
        for(y = [-1, 1]) {
            translate([0, y * ((switchbox_w + wall) / 2), 0]) {

                for(x = [-1, 1]) {

                    translate([ x * (switchbox_l / 2 + wall) * 0.75, 0, 0]) {

                        rotate([90, 0, 0]) {
                            pip();
                        }
                    }
                }
        
            }
        }
    }
}








translate([0, 0, switchbox_h / 2 + wall/2]) rotate([0, 180, 0]) frame();


module clip() {

    color("CornflowerBlue") 
    for(i = [1, -1])
    hull() {
        translate([ i * (switchbox_l / 2 + wall) * 0.75, (switchbox_w + wall * 2) / 2, 0]) 
        mcube(center = [0, 1, 0], size = [inch / 4, wall * 2,  wall], chamfer = inch / 8 / 8);

        translate([ i * -(switchbox_l / 2 + wall) * 0.75, -(switchbox_w + wall * 2) / 2, 0]) 
        mcube(center = [0, -1, 0], size = [inch / 4 , wall* 2,  wall], chamfer = inch / 8 / 8);
    }

        
    for(x = [1, -1])
    for(y = [1, -1])
    translate([ x * (switchbox_l / 2 + wall) * 0.75, y * (switchbox_w + wall * 2) / 2, 0]) 
    {

        translate([0, 0, wall + switchbox_h / 2])
        rotate([90, 0, 0])
        color("blue")
        pip(antipip = false, pipness = 2, square = true);

        
        color("lavender") 
        hull() {
//        union(){
            color("orange")
            translate([0, y * wall/2, wall + switchbox_h / 2])
            rotate([90, 0, 0])
            pip(antipip  = false, pipness = 1, square = true);

            color("Crimson")
            intersection() {
  //          translate([0, y * wall, 0])
//            rotate([90, 0, 0])
//            pip(antipip  = false, pipness = 2);
            
            mcube([wall * 2, wall*2, wall], center = [0, y, 1], chamfer = inch / 8 / 8);
            }
        }


    }
}


//translate([0, 0, -wall/2])
translate([0, 50, wall/2])
clip();

