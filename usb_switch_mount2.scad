include <libmose/mdefaults.scad>
use <libmose/mbox.scad>
use <libmose/mcylinder.scad>

wall = inch / 8;
chamfer = wall / 4;

switchbox_l = 61;
switchbox_h = 14;
switchbox_w = 26;

cover_l = 19;

cable_thickness = 5;


difference() {

union(){
for(y = [-1, 1]) {
    translate([0, y * (cable_thickness / 2), 0]) {


        mbox(size = [switchbox_l + wall * 2, ((switchbox_w - cable_thickness) / 2 + wall), switchbox_h + wall],
            center = [0, y, 1],
            wall_thickness = wall,
            chamfer = chamfer, 
            xpos = true,
            xneg = true,
            ypos = y > 0 ? true : false,
            yneg = y < 0 ? true : false,
            zpos = false,
            zneg = false,
            manifold_underlap = false,
            manifold_overlap = false,
            color = "orange"
        );


translate([0, 0, -switchbox_h])
        mbox(size = [switchbox_l + wall * 2, ((switchbox_w - cable_thickness) / 2 + wall), switchbox_h + wall],
            center = [0, y, 1],
            wall_thickness = wall,
            chamfer = chamfer, 
            xpos = true,
            xneg = true,
            ypos = y > 0 ? true : false,
            yneg = y < 0 ? true : false,
            zpos = false,
            zneg = false,
            manifold_underlap = false,
            manifold_overlap = false,
            color = "lightblue"
        );
        




for(x = [-1, 1]) {

            translate([ x * (switchbox_l / 2 + wall), 0, 0]) {
color("lightgreen") hull()
                mbox(size = [cover_l + wall , ((switchbox_w - cable_thickness) / 2 + wall), switchbox_h + wall],
                    center = [-x, y, 1],
                    wall_thickness = wall,
                    chamfer = chamfer, 
                    xpos = x > 0 ? true : false,
                    xneg = x < 0 ? true : false,
                    ypos = y > 0 ? true : false,
                    yneg = y < 0 ? true : false,
                    zpos = false,
                    zneg = false,
                    manifold_underlap = false,
                    manifold_overlap = false,
                    color = "lightgreen"
                );
                
            }
        }
        

        
    



    
    }

}


}

    // pips
    

    union() {
        for(y = [-1, 1]) {
            translate([0, y * ((switchbox_w + wall) / 2), switchbox_h / 2]) {

                for(x = [-1, 1]) {

                    translate([ x * (switchbox_l / 2 + wall) * 0.75, 0, 0]) {

                        rotate([90, 0, 0]) {
                            mcylinder(d = inch / 4, h = 10, $fn = 180, center = true, chamfer = inch / 8 / 4);
                        }
                    }
                }
        
            }
        }
    }
}


        mbox(size = [switchbox_l + (wall * 2), switchbox_w + wall * 2, switchbox_h + wall * 1],
            center = [0, 0, 1],
            wall_thickness = wall,
            chamfer = chamfer, 
            xpos = false,
            xneg = false,
            ypos = false,
            yneg = false,
            zpos = true,
            zneg = false,
            manifold_underlap = false,
            manifold_overlap = false,
            color = "pink"
        );





