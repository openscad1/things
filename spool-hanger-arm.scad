use <libopenscad/mtube.scad>;
use <libopenscad/mcube.scad>;
use <libopenscad/mcylinder.scad>;

$fn = 90;

inch = 25.4;

interior_width = inch * 0.31;
exterior_width = interior_width + inch * 0.25;




clip_height = inch * 0.5;

default_chamfer = (exterior_width - interior_width) / 8;
wall_thickness = (exterior_width - interior_width) / 2;

module horseshoe(height, id, od, foot, align = [0, 0, 0], color = undef, chamfer = default_chamfer, debug = undef) {

    module thing() {


        color("orange") {
            rotate([0, 0, 180]) {
                mtube(h = height, od = od, id = id, chamfer = chamfer,  angle = 180);
            }
        }
        
        color("lightblue") {
            for(y = [-1, 1]) {
                translate([0, y * od / 2, 0]) {
                    hull() {
                        mcylinder(h = height, d = (od - id) / 2, align = [0, -y, 0], chamfer = chamfer);
                        translate([foot, 0, 0]) {
                            mcylinder(h = height, d = (od - id) / 2, align = [0, -y, 0], chamfer = chamfer);
                        }
                    }
                }
            }
        }

    }

    translate([(od / 2) * align.x, (od / 2) * align.y, (height / 2) * align.z]) {
        if(debug != undef) {
            color("red") {
                cube([id, id, height * 1.5], center = true);
            }
        }

        if (color == undef) {
            thing();
        } else {
            color(color) {
                thing();
            }
        }
    }
}


module oval(color = undef) {
    // a pair of horseshoes facing each other to make an oval

    rotate([0, 0, -90]) {
        // down-facing horseshoe
        horseshoe(height = clip_height, id = interior_width, od = exterior_width, foot = 7, align = [1, 0, 0], color = color ? color : "pink");
    }

    translate([0, -(exterior_width - interior_width), 0]) { // align interior edges
        
        translate([0, -(11.75 + 4), 0]) { // 11.75mm bolt height, 2mm top and bottom to allow for curvature
            rotate([0, 0, 90]) {
                horseshoe(height = clip_height, id = interior_width, od = exterior_width, foot = 7, align = [1, 0, 0], color = color ? color : "orange");
            }
        }
    }
}




for(y = [0, 1.25, 2.5, 3.75, 5]) {

    translate([0, -inch * y, 0]) {

        oval(color = "fuchsia");

    }

}




module cutout() {

    translate([0, -wall_thickness/2, 0]) {

        hull() {
            mcylinder(d = exterior_width - wall_thickness, h = clip_height * 2, align = [0, -1, 0]);
            translate([0, -(11.75 + 4), 0]) { // 11.75mm bolt height, 2mm top and bottom to allow for curvature
                translate([0, -wall_thickness, 0]) {
                    mcylinder(d = exterior_width - wall_thickness, h = clip_height * 2, align = [0, 1, 0]);
                }
            }
        }
        
    }

}


//frame
translate([0, 0, -4 * 0])
difference() {
    hull() {
        translate([0, (wall_thickness), 0]) {
            mcylinder(h = clip_height, d = exterior_width + wall_thickness * 2, chamfer = default_chamfer, align = [0, -1, 0]);
        }

        translate([0, -inch * 5, 0]) {
            translate([0, -(11.75 + 4), 0]) { // 11.75mm bolt height, 2mm top and bottom to allow for curvature
                translate([0, -(wall_thickness * 3), 0]) {
                    mcylinder(h = clip_height, d = exterior_width + wall_thickness * 2, chamfer = default_chamfer, align = [0, 1, 0]);
                }
            }
        }
    }

    union() {

        // cutouts

        for(y = [0, 1.25, 2.5, 3.75, 5]) {

            translate([0, -inch * y, 0]) {
                cutout();
            }

        }
    }
}




