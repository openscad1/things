use <libopenscad/mtube.scad>;
use <libopenscad/mcube.scad>;
use <libopenscad/mcylinder.scad>;

$fn = 90;

inch = 25.4;

interior_width = inch * 0.31;
exterior_width = interior_width + inch * 0.25;




clip_height = inch * 0.5;



module horseshoe(height, id, od, foot, align = [0, 0, 0], color = undef, chamfer = -1, debug = undef) {

    chamfer = (od - id) / 8;

    module thing() {


        color("orange") {
            rotate([0, 0, 180]) {
                mtube(h = height, od = od, id = id, chamfer = chamfer,  angle = 180);
            }
        }
        
        color("lightblue") {
            for(y = [-1, 1]) {
                translate([0, y * od / 2, 0]) {
                    hull() {
                        mcylinder(h = height, d = (od - id) / 2, align = [0, -y, 0], chamfer = chamfer);
                        translate([foot, 0, 0]) {
                            mcylinder(h = height, d = (od - id) / 2, align = [0, -y, 0], chamfer = chamfer);
                        }
                    }
                }
            }
        }

    }

    translate([(od / 2) * align.x, (od / 2) * align.y, (height / 2) * align.z]) {
        if(debug != undef) {
            color("red") {
                cube([id, id, height * 1.5], center = true);
            }
        }

        if (color == undef) {
            thing();
        } else {
            color(color) {
                thing();
            }
        }
    }
}


module oval(color = undef) {
    // a pair of horseshoes facing each other to make an oval

    rotate([0, 0, -90]) {
        // down-facing horseshoe
        horseshoe(height = clip_height, id = interior_width, od = exterior_width, foot = 7, align = [1, 0, 0], color = color ? color : "pink");
    }

    translate([0, -(exterior_width - interior_width), 0]) { // align interior edges
        
        translate([0, -(11.75 + 4), 0]) { // 11.75mm bolt height, 2mm top and bottom to allow for curvature
            rotate([0, 0, 90]) {
                horseshoe(height = clip_height, id = interior_width, od = exterior_width, foot = 7, align = [1, 0, 0], color = color ? color : "orange");
            }
        }
    }
}


// top open horseshoe - attaches to hanger rod
translate([0, (exterior_width - interior_width)/4, 0]) {
    horseshoe(height = clip_height, id = interior_width, od = exterior_width, foot = (exterior_width / 2) - (exterior_width - interior_width)/4, align = [0, 1, 0], color = "lightblue");
}


// through hole for bolt
translate([0, -(exterior_width - interior_width)/4, 0]) {
    oval();
}


// connect the clip and the bolt hole
color("red") mcube([exterior_width/2, 3*(exterior_width - interior_width)/2 - 0.01, clip_height], align = [0, 0, 0], chamfer = (exterior_width - interior_width) / 8);

