// NOSTL

include <libmose/mdefaults.scad>
use <libmose/mcylinder.scad>
use <libmose/mtube.scad>
use <libmose/mcube.scad>
use <libmose/mutil.scad>

include <threadlib/threadlib.scad>

//bolt("M20", turns=15);


default_text_size = 7;

module orbit_ring(h, d, arc = 140, $fn = 360) {
    // "positive" near base
    color("red") {
        for(z = [0, 180]) {
            rotate([0, 0, z]) {
                    for(a = [-(arc * 0.9) / 2 , (arc * 0.9) / 2]) {
                        rotate([0, 0, a]) { 
                            translate([((inch * d) / 2) - ((inch / 8) / 2), 0]) sphere(d = inch / 8);
                        }
                    }

                    rotate([0, 0, -(arc * 0.9) / 2])
                    rotate_extrude(angle = (arc * 0.9)) {
                        translate([((inch * d) / 2) - ((inch / 8) / 2), 0, 0]) {
                            circle(d = inch / 8);
                        }
                    }

            }
        }
    }
}
        
        


module inner(h, d, arc = 140, orbit = false, $fn = 360) {
    
    difference() {
        
        union() {

            for(zz = [0, 180]) {
                
                rotate([0, 0, zz]) {



                    color("lightblue")
                    mtube(h = inch * h, od = inch * d, id = inch * d - inch * 1/4, center = [0, 0, 1], chamfer = (inch / 8) / 4, angle = arc, chamfer_ends = true);

       
                    

                }
            }



            color("lightgreen")
            mcylinder(h = inch / 8, d = inch * d, chamfer = (inch / 8) / 4, center = [0, 0, 1]);

            color("pink")
            mcylinder(h = inch * h, d = inch * 0.75, chamfer = (inch / 8) / 4, center = [0, 0, 1]);

    
            translate([0, 0, (inch / 8) * 3]) {
                if(orbit) {
                    orbit_ring(h = 2 - (1/8), d = d + (1/16) - (1/64), $fn = 60);
                }
            }
        }
        
        
        
        
        union() {
            translate([0, 0, ((2 - (1/8)) * inch) - ((inch / 8) * 3)]) {
                if(orbit) {
                    orbit_ring(h = 2 - (1/8), d = d + (3/16), $fn = 60);
                }
            }
            
            



            translate([0, 0, 1]) {
                rotate([180, 0, 0]) {
                    linear_extrude(2) {
                        _points = 3;
                        _s = default_text_size;
                        translate([0,  _s * 1.5, 0])   text(str("h: ", mprecision(h, _points)), size = _s, halign = "center", valign = "center");
                                               text(str("d: ", mprecision(d, _points)), size = _s, halign = "center", valign = "center");
                        translate([0, -_s * 1.5, 0]) text(str("a: ", mprecision(arc, _points)), size = _s, halign = "center", valign = "center");

                    }
                }
            }
        }
        
        
        
        
    }



}



module outer(h, d, arc = 140, orbit = false, $fn = 360) {
    
    difference() {
        
        union() {

            for(zz = [0, 180]) {
                
                rotate([0, 0, zz]) {

                    color("lightblue")
                    mtube(h = inch * h, od = inch * d, id = inch * d - inch * 1/4, center = [0, 0, 1], chamfer = (inch / 8) / 4, angle = arc, chamfer_ends = true);


                }
            }


            color("lightgreen")
            mcylinder(h = inch / 8, d = inch * d, chamfer = (inch / 8) / 4, center = [0, 0, 1]);
            
            translate([0, 0, (inch / 8) * 4]) {
                if(orbit) {
                    orbit_ring(h = 2 - (1/8), d = d - (1/16) + (1/64), $fn = 60);
                }
            }

        }



        union() {
            translate([0, 0, ((2 - (1/8)) * inch) - ((inch / 8) * 2)]) {
                if(orbit) {
                    orbit_ring(h = 2 - (1/8), d = d - (3/16), $fn = 60);
                }
            }
        }
        
            translate([0, 0, 1]) {
                rotate([180, 0, 0]) {
                    linear_extrude(2) {
                                                _points = 3;

                        _s = default_text_size;
                        translate([0,  _s * 1.5, 0])   text(str("h: ", mprecision(h, _points)), size = _s, halign = "center", valign = "center");
                                               text(str("d: ", mprecision(d, _points)), size = _s, halign = "center", valign = "center");
                        translate([0, -_s * 1.5, 0]) text(str("a: ", mprecision(arc, _points)), size = _s, halign = "center", valign = "center");

                    }
                }
            }        
        
        
    }

}



module base(d, $fn = 360, angle = 30) {
    color("cyan") {
        for(z = [-1, 1])  
        rotate([0, 0, z * 90]) {

            el = 2 - 1/4 + orbital_allowance();

            mtube(h = (1 + el) * inch / 8, od = inch * d, id = inch * (d - 1/4), chamfer = (inch / 8) / 4, center = [0, 0, 1], angle = angle, chamfer_ends = true);
            
            translate([0, 0, el * (inch / 8)]) {
                // lip
                mtube(h = inch / 8, od = inch * d, id = (inch * (d - 1/4)) - (inch / 8)/2, chamfer = (inch / 8) / 4, center = [0, 0, 1], angle = angle, chamfer_ends = true);
                
            }
        }
    }
    color("grey") {
        // outer ring
        mtube(h = inch * 1/8, od = inch * d, id = inch * (d - 3/4), chamfer = (inch / 8) / 4, center = [0, 0, 1]);
    }
    
    // screw hole
    screw_hole_diameter = 5;
    color("LightSteelBlue") {
        mtube(h = inch / 8,       od = inch * 3/4, id = screw_hole_diameter, chamfer = (inch / 8) / 2, center = [0, 0, 1]);
    }
    color("CornflowerBlue") {
        mtube(h = (inch / 8) / 2, od = inch * 3/4, id = screw_hole_diameter,                           center = [0, 0, 1]);
    }
    color("PaleTurquoise") {
        mtube(h = (inch / 8),     od = inch,       id = inch * 1/2,          chamfer = (inch / 8) / 4, center = [0, 0, 1]);
    }
    
    // arms
    rotate([0, 0, 90]) {
        for(z = [-1, 1]) {
            rotate([0, 0, z * 90]) {
                translate([inch / 4, 0, 0]) {
                    mcube([inch * (d/2) - (inch * 1/2), inch / 2, inch / 8], chamfer = (inch / 8) / 4, center = [1, 0, 1]);
                }
            }
        }
    }
}



function jessie_shrink() = (
    // Jessie filament shrinks quite a bit, so shrink the inside a bit
    (1/64)
);


function orbital_allowance() = (
    // trim the top of the inner part to allow the orbital rings to engage
    (1/64)
);





union() {
    {
        // inner case
            translate([-150, 0, 0 ]) 

        inner(h = 2 - (1/8), d = 3 - (1/4), orbit = true, $fn = 60);
    }


translate([0, 0, -(inch * 1/8)]) {
    base(d = 3, $fn = 60, angle = 35 );
}

    translate([150, 0, 0 ]) 
    translate([0, 0, inch * 2 ]) 
    rotate([180, 0, 0]) 
    {
        // outer case
        outer(h = 2, d = 3, orbit = true, $fn = 60);
    }

}




