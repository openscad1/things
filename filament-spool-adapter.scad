use <libthread/adrianschlatter-threadlib.scad>
use <libmose/mtube.scad>
use <libmose/mcube.scad>
use <libmose/mcylinder.scad>


inch = 25.4;

outer_thread_descriptor = "M36";
inner_thread_descriptor = "M20";

    prusa_spool_core_diameter = 51;
    prusa_spool_core_height = inch;
    prusa_spool_rim_height = 4.5;
    prusa_spool_inner_ring_thickness = 3;
    prusa_spool_core_width = inch / 4 + .932;

    prusa_spool_flange_width = inch;
    prusa_spool_flange_height = inch / 4;

slop = 0.975;

module prusa_female_adapter() {

    turn_height = thread_pitch(str(outer_thread_descriptor, "-int"));


    {
        // core
        
        turn_reduction = 2;
        
        mtube(
            h = prusa_spool_core_height - (turn_height * turn_reduction), 
            od = prusa_spool_core_diameter, 
            id = support_diameter(str(outer_thread_descriptor, "-int")),
            chamfer = prusa_spool_core_width / 4, 
            center = [0, 0, 1], 
            color = "lightblue", 
            $fn = 360
        );
    }
    
    {
        // flange
        mtube(
            h = prusa_spool_flange_height / 2, 
            od = prusa_spool_core_diameter + (prusa_spool_inner_ring_thickness + prusa_spool_core_width) * 2, 
            id = (prusa_spool_core_diameter - prusa_spool_core_width), 
            chamfer = prusa_spool_core_width / 4 / 2, 
            center = [0, 0, 1], 
            color = "salmon", 
            $fn = 360
        );
    }

    
    {
        // threads
        turns = (prusa_spool_core_height / turn_height) - 2 - turn_reduction;
        translate([0, 0, turn_height]) {
            color("orange")
            nut(outer_thread_descriptor, turns = turns, Douter = prusa_spool_core_diameter);
        }
    }



    {
        // nub
        translate([-(prusa_spool_core_diameter / 2 + 2 ), 0, 0]) {
            mcube([2 + 3,2,7], chamfer = 1/2, center = [1, 0, 1], color = "fuchsia");
        }
    }
    
    
}


module prusa_male_adapter() {
    
    turn_height = thread_pitch(str(outer_thread_descriptor, "-ext"));
    turns = (prusa_spool_core_height / turn_height);

        isd = support_diameter(str(inner_thread_descriptor, "-int"));
        osd = support_diameter(str(outer_thread_descriptor, "-ext"));
    h2 = (turn_height * turns) + prusa_spool_flange_height;

    {
        // central outer threaded post            

        difference() {
            union() {
                // bolt
                translate([0, 0, turn_height /2 ]) {
                    color("lightgreen")
                    scale([slop, slop, 1])
                    bolt(outer_thread_descriptor, turns = turns - 1);
                }
            }

            union() {
                translate([0, 0, -1]) {
                    mcylinder(d = isd + ((osd - isd) / 2), h = h2 + 2);
                }
            }
        }
    }



    {
        // central inner threaded post
        
        union() {

            inner_turn_height = thread_pitch(str(inner_thread_descriptor, "-int"));
            inner_turns = (h2 / inner_turn_height);

            translate([0, 0, inner_turn_height ]) {
                // central thread
                color("orangered")
                scale([slop, slop, 1])
                nut(inner_thread_descriptor, turns = inner_turns - 2 -2 , Douter = isd + ((osd - isd) / 2));
            }
            
            {
                // surround for central thread
                translate([0, 0, (h2)/2 - inner_turn_height]) {
                    mtube(id = isd * slop, od = osd * slop, chamfer = ((osd * slop) - isd) / 8, h = h2 - inner_turn_height * 2, center = true, color = "mediumvioletred", $fn = 360);
                }
            }

        }
    }







    {
        // knob
        
        _inner_ring_height = prusa_spool_core_height - prusa_spool_rim_height; 
        echo("_inner_ring_height:", _inner_ring_height);
        
        difference() {
            union() {
                // inner ring
                mtube(
                    h = _inner_ring_height, 
                    od = prusa_spool_core_diameter + (prusa_spool_inner_ring_thickness + prusa_spool_core_width) * 2, 
                    id = prusa_spool_core_diameter + (prusa_spool_inner_ring_thickness                         ) * 2, 
                    chamfer = prusa_spool_core_width / 3, 
                    center = [0, 0, 1], 
                    color = "cornflowerblue", 
                    $fn = 360
                );
                
                    {
        // flange with a hole

        difference() {
            union() {
                // flange
                mcylinder(
                    h = prusa_spool_flange_height, 
                    d = prusa_spool_core_diameter + (prusa_spool_inner_ring_thickness + prusa_spool_core_width) * 2, 
                    chamfer = prusa_spool_core_width / 4, 
                    center = [0, 0, 1], 
                    color = "salmon", 
                    $fn = 360
                );
            }
            union() {
                translate([0, 0, -1]) {
                    mcylinder(d = isd + ((osd - isd) / 2), h = prusa_spool_flange_height + 2);
                }
            }
        }
    }



            }

            
            union() {
                // grippies
                for(a = [0 : 30 : 360]) {
                    rotate([0, 0, a]) {
                        _h = 2 * ((prusa_spool_core_height - prusa_spool_rim_height) - prusa_spool_core_width);
                        translate([(prusa_spool_core_diameter / 2) + (prusa_spool_inner_ring_thickness + prusa_spool_core_width) + 2, 0, (_h / 2) + ((_inner_ring_height) - _h) / 2]) {
                            mcylinder(d = 8, chamfer = 2, h = _h, center = [0, 0, 0], $fn = 360);
                        }
                    }
                }
            }
        }
        
    }

    {
        // nub
        translate([-(prusa_spool_core_diameter / 2 + 4 + 3), 0, 0]) {
            mcube([3.5 + 3,2,prusa_spool_core_height - prusa_spool_rim_height], chamfer = 1/2, center = [1, 0, 1], color = "fuchsia");
        }

    }


}

module jessie_male_adapter() {

    jessie_spool_inner_diameter = 57;

            {

            turns = 6;
            osd = support_diameter(str(inner_thread_descriptor, "-ext"));
            turn_height = thread_pitch(str(inner_thread_descriptor, "-ext"));
            
            translate([prusa_spool_core_diameter, 0, 0])  {
                translate([0, 0, 70 + inch / 4 - turn_height]) { 
                    translate([0, 0, turn_height]) 
                    scale([slop, slop, 1])scale([slop, slop, 1]) {

                        color("seagreen") 
                        bolt(inner_thread_descriptor, turns = turns);
                            
                        color("gold")
                        translate([0, 0, -turn_height * 1])
                        mcylinder(d = osd,  h = turn_height * (turns + 2), chamfer = osd / 16);
                    }
                        
                    translate([0, 0, turn_height]) { 
                        mtube(h = 70 + inch / 4, od = 57, id = 57 - (inch / 4) * 2, chamfer = inch / 16, $fn = 360, center = [0, 0, -1]);
                        mcylinder(d = 57, h = inch / 4, center = [0, 0, -1], chamfer = inch / 8, color = "bisque");
                    }

                }
                mtube(od = 140, id = 57 - (inch / 4) * 2, h = inch / 4, chamfer = inch / 16, color = "mediumslateblue", $fn = 360);
            }
            
        }

    }
    
    
module    prusa_mini_stand_lift() {
    
    lip_height = 4;
    lip_thickness = 5;
    post_height = 35;
    post_inside_diameter = 36;

    $fn = 360;

//mcube([25.4, 25.4, 25.4], center = true, color = "slateblue");
    
difference() {
//union() {
    union() {
        // post
        mcylinder(d = post_inside_diameter + lip_thickness * 2, h = post_height, chamfer = lip_thickness / 4);
        mtube(od=post_inside_diameter + lip_thickness * 2, id=post_inside_diameter, h=post_height + lip_height, chamfer = lip_thickness / 4, center = [0, 0, 1]);
    }

    union() {
        
        /*
        // dimples
        for(z = [20 : 20 : post_height - 20]) {
            for(a = [0 : 30 : 360]) {
                rotate([0, 0, a]) {
                    translate([(post_inside_diameter / 2) + lip_thickness * 2, 0, z]) {
                        sphere(r = lip_thickness * 1.5);
                    }
                }
            }
        }
        */
        
        for(a = [0 : 45 : 360]) {     
                rotate([0, 0, a]) {
            translate([(post_inside_diameter / 2) + lip_thickness * 1.1, 0, (post_height + lip_height) / 2]) {
                    echo(a);
                    mcylinder(h=(post_height + lip_height) * 0.8, d= lip_thickness, chamfer = lip_thickness / 4, center = true);
                }
            }
        }
}

}
}


module filament_sensor_offset_plate() {
    
    center2center = 36.25;
    offset = 10;
    
    x = center2center + offset;
    y = 25.4 + offset; 
    z = 5; 
    c = z/6;
    
    
    osd = support_diameter(str("M3-ext"));
    echo(osd);

    difference() {
        union() {    
            mcube([x, y, z], chamfer = c, color = "steelblue", center = true);
        }

        union() {

            for(_y = [-1, 1]) {
                for(_x = [-1, 1]) {
                    translate([_x * center2center / 2, 25.4 / 2 * _y, 0]) {
                        mcylinder(d = _y > 0 ? 3.25 : ((3 - osd) / 2) + osd, h = z * 2, center = true);
                    }
                }
            }

        }
    }
}


//filament_sensor_offset_plate();


//intersection() {
union() {
    union() {
//        mcube([2000, 2000, 2000], center = [0, 1, 0], color = "red");
//        translate([0, 0, 5]) mcube([1000, 1000, 5], center = [0, 0, 1], color = "cyan");
    }
    
    union() {
//        translate([prusa_spool_core_diameter + prusa_spool_flange_width * 1.5, 0, 0]) 
        {
//            prusa_female_adapter();
        }


//        translate([0, 0, prusa_spool_core_height + 1.35])
//        rotate([180, 0, 0])
        {
            prusa_male_adapter();
        }




    
}


//jessie_male_adapter();

//prusa_mini_stand_lift();

}
