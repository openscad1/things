use <cable-box.scad>


inch = 25.4;


box_lid(x_extent = 5, y_extent = 5, z_extent = 1, corner_radius = 15, y_notch_width = inch * 3/4, wall_thickness = inch / 4);
