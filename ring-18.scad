include <libopenscad/mtube.scad>;


// NB: slice so that the entire ring is perimeters

od = 18;

id = od * (2/3);
wall = od * (1/4);
chamfer = wall * (1/6);

$fn = 360;

mtube(h = wall, od = od, id = id, align = [0, 0, 0], chamfer = chamfer);



