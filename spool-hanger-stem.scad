include <libopenscad/mnut.scad>;

//cutaway = 1;
//debug = true;

$fn = 90;

thread_size = 20;

flange_height = 2.5;
flange_reach = 2;

module stem() {
    union() {
        translate([0, 0, 0.01]) {
            mbolt(thread_size = thread_size, turns = 8, align = [0, 0, 1]);
        }
        translate([0, 0, flange_height])  {
            plug();
        }
    }
}


for (x = [-1, 1]) {
    translate([x * 40, 0, 0]) {
        rotate([0, x * 90 * -1, 0])
        intersection() {
            stem();
            mcube([100, 100, 100], align = [x, 0, 0], color = "red");
        }
    }
}

module plug() {
    color("lightblue") {
        translate([0, 0, 0.01]) {
            mcylinder(d = thread_size, h = flange_height * 2, align = [0, 0, 0]);
        }
    }
    color("orange") {
        translate([0, 0, -flange_height/2]) {
            mcylinder(d = thread_size + flange_reach * 2, h = flange_height, align = [0, 0, 0]);
        }
    }
}


/*
plug_scale = 1.025;
rotate([0, 180, 0])
color("lightgreen")
translate([40, 40, -flange_height]) {
    difference() {
        mcube([40, 40, (flange_height * 2) - 0.01], align = [0, 0, 0]);
        translate([0, 0, -0.01]) {
            scale([plug_scale, plug_scale, 1.2]) {
                plug();
            }
        }
    }
}


*/